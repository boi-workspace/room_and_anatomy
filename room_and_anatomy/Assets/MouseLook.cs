﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {
	public float mouse_sensitivity = 100.0f;
	public float clamp_angle = 80.0f;

	private float rot_y = 0.0f; // rotation around the up/y axis
	private float rot_x = 0.0f; // rotation around the right/x axis

	void Start () {
		Vector3 rot = transform.localRotation.eulerAngles;
		rot_y = rot.y;
		rot_x = rot.x;
		Debug.Log("MouseLook initialised");
	}

	void Update () {
		float mouse_x = Input.GetAxis ("Mouse X");
		float mouse_y = Input.GetAxis ("Mouse Y");

		rot_y += mouse_x * mouse_sensitivity * Time.deltaTime;
		rot_x += mouse_y * mouse_sensitivity * Time.deltaTime;

		rot_x = Mathf.Clamp (rot_x, -clamp_angle, clamp_angle);

		Quaternion local_rotation = Quaternion.Euler (rot_x, rot_y, 0.0f);
		transform.rotation = local_rotation; // TODO needed?
	}
}
