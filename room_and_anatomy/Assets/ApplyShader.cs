﻿/* requires user to asign shader for object instance */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyShader : MonoBehaviour {
	public Shader clip_shader;
	public Renderer rend;

	void Start() {
		rend = GetComponent<Renderer>();
		if (clip_shader != null)
			rend.material.shader = clip_shader;
	}
}