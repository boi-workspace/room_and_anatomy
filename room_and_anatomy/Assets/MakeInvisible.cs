﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeInvisible : MonoBehaviour {
	void Start () {
		GetComponent<Renderer>().enabled = false;
	}
}
